# Clases de desarrollo

En este proyecto se encuentran ejemplos y explicaciones de HTML, CSS y JavaScript de acuerdo a cada clase realizada.
Para poder utilizar este proyecto, deben seguirse las siguientes instrucciones:

## Instrucciones

Las explicaciones y ejemplos de cada clase estarán en ramas independientes. Las ramas son como diferentes versiones del proyecto, por lo tanto habrá una versión para cada clase.
Para seleccionar la rama de la clase deseada debe hacerse click en el menú desplegable que contiene la palabra `master` (se muestra con una flecha naranja en el screenshot).

![screenshot](./assets/project_screenshot.png)

Al hacer click en el menú desplegable, se mostrará una lista de opciones con el siguiente formato:

- `clase_01`
- `clase_02`
- `...`

Solamente debe seleccionarse la opción correspondiente a la clase deseada, y en la tabla inferior se mostrarán los archivos de esa clase, en conjunto con la explicación adecuada.
